﻿using System;

namespace exercise_20
{
    class Program
    {
        static void Main(string[] args)
        {
         //Start the program with Clear();
         Console.Clear();

         var num1 = "";
         var num2 = "";

         Console.WriteLine("Please enter a number and press enter");
            num1 = Console.ReadLine();
            Console.WriteLine("");
         Console.WriteLine("Now enter another number and press enter");
            num2 = Console.ReadLine();

         Console.WriteLine($"  {num1} + {num2} = {num1 + num2}");

         
         //End the program with blank line and instructions
         Console.ResetColor();
         Console.WriteLine();
         Console.WriteLine("Press <Enter> to quit the program");
         Console.ReadKey();
        }
    }
}
